package com.example.sourceit_android_homework_lesson1;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

//        TextView text = findViewById(R.id.text);
//        text.setText("Hello SourceIt");

        final TextView textView = findViewById(R.id.text);

        Button buttonToWriteText = findViewById(R.id.buttonToIncreaseNumber);
        buttonToWriteText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int number = Integer.parseInt(textView.getText().toString());
                textView.setText(Integer.toString(number + 1));
            }
        });

        Button buttonToDeleteText = findViewById(R.id.buttonToDecreaseNumber);
        buttonToDeleteText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int number = Integer.parseInt(textView.getText().toString());
                textView.setText(Integer.toString(number - 1));
            }
        });
    }
}

